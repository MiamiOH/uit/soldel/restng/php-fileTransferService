# RESTng File Transfer Service

The File Transfer service provides resources for interacting with remote file
storage systems. File contents are contained in REST requests, so there will be
practical limits to the supportable file size. This service is not intended to
support large file transfers.

File contents are base64 encoded so binary file types are supported. 

## Supported Services

The FileTransfer service is intended to provide a somewhat consistent interface to
transfer files using different methods. 

### Google Drive

Google uses OAuth 2 for managing access to APIs, including Drive. Setting this up
for a script to use takes a bit of work. There may be better ways to do this, but
after some trial and error, this seems to work. Google changes APIs on a regular
basis and I found outdated documentation and very little to clarify this process.

#### Step 1: Create an entity account

We recommend the use of an entity account to contain the folder used for uploading.
The folder can then be shared with any users needing access to the data. Instructions for creating a Entity Account are located here: [Request an Entity Account](https://miamioh.teamdynamix.com/TDClient/1813/Portal/Requests/ServiceOfferingDet?ID=532)

#### Step 2: Create a project and enable the Drive API

1. Use the Google API dashboard to start a new project, using your entity account to
login: https://console.developers.google.com/apis/dashboard
2. Create a project new project. The name of the project doesn't matter, but you should name it something relevant to the application that will be using it. 
3. Enable the Google Drive API
    * Go to the Dashboard for the project(https://console.developers.google.com/apis/dashboard)
    * Select Enable APIs & services
    * At the top click "Enable APIS and Services"
    * Search for Google Drive API
    * Click Enable

#### Step 3: Create Service Account

1. Go to the Dashboard for the project (https://console.developers.google.com/apis/dashboard)
2. Select Credentials on the side bar.
3. Click "Create Credentials" and select Service Account.
4. The service account name is up to you. This will create a new entity within the scope of your project
    * The service account ID will look similar to a email address (ex. serviceid@myproject.iam.gserviceaccount.com). Make note of this for granting access to the Shared Drive later.
5. You do not need to grant any roles for the project
6. Leave the key type set to JSON and make sure to save the resulting file since this is the only chance you get. This JSON will be needed when you create the parameter store later.

#### Create and share an upload folder

1. Login to Google Drive using the entity account. 
2. Create a new ***Shared Drive*** (not a normal Google Drive Folder)
   * It is recommended you also at this time create a Shared drive specifically for DEVL/TEST usage. Follow same steps for DEVL/TEST folder.
3. Give the service account Manager access to Shared Drive. Also share it with any other users who need access to either upload or pick up files.
4. Enter into the Shared Drive Folder to retrieve the ID of the folder from the URL. The ID is the last item in the URL and make note of it for later use.
   * The drive URL will be in the following format: https://drive.google.com/drive/folders/{id}
    * If you are setting up a DEVL/TEST folder, you will need to make note of those IDs as well.

#### Create the datasource

Follow instructions given here: [Procedure: Training / Create a new RESTng datasource](https://miamioh.teamdynamix.com/TDClient/1813/Portal/KB/ArticleDet?ID=146395)

### Amazon S3

Amazon S3 is an easy to use cloud storage system. Our experience so far has been
limited to a vendor provided "bucket" for uploading, but the setup should be easy
to do via the S3 admin UI.

The S3 transport uses a RESTng datasource to acquire connection information.
The S3 service owner should provide the Access Key ID, Secret Access Key, region and
bucket to be used. Set these in the datasource as follows:

| Datasource Field | Value                |
|------------------|-------------------|
| user             | Access Key ID     |
| password         | Secret Access Key |
| host             | region            |
| database         | bucket            |

Folder paths within the bucket are only supported as part of the file name when uploaded.
S3 will create folders which do not exist.

## Authorizing access

Authorization to use File Transfer services is done via Authman. Modules are used to 
represent destinations. The module name must exactly match the destination resource 
parameter. The supported keys for authorization are 'list', 'get', 'create' and 'delete'.

Given a `DepartmentReports` destination, we would expect to request a retrieve the list
of files to be:

```
https://ws.apps.miamioh.edu/api/fileTransfer/DepartmentReports
```

The 'File Transfer' application in AuthMan must have a module named exactly 
'DepartmentReports' and the authenticated user must be given the 'list' permission.

## Using the Resources

The File Transfer service operates on a File data model. The model is visualized as:

```json
{
  "id": "int",
  "name": "string",
  "mimeType": "string",
  "content": "string"
}
```
The `id` attribute may be omitted when sending a create request.

The `content` attribute is only populated when getting a specific file. It will be empty 
when used in a file list collection.

The `content` attribute *MUST* be base64 encoded. When sending a create request, you
should perform a binary safe read of the file and base64 encode the result to use as
the content value. When requesting a file object, you must base64 decode the result
before using it.

The available resources are:

```
GET /fileTransfer/v1/{destination}
POST /fileTransfer/v1/{destination}
GET /fileTransfer/v1/{destination}/{id}
DELETE /fileTransfer/v1/{destination}/{id}
```
