<?php

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7;

require_once __DIR__ . '/../vendor/autoload.php';

$apiBase = 'https://localhost';
$GLOBALS['token'] = 'file-transfer';

$GLOBALS['client'] = new \GuzzleHttp\Client([
    'base_uri' => $apiBase,
    'verify' => false,
]);

// TODO Add query filter for GET file list (how to make that transport agnostic?)
$response = send(createRequest('GET', '/api/fileTransfer/v1/SolDel_FileTransfer'));
$files = json_decode($response->getBody()->getContents(), true)['data'];

$headers = ['#', 'Name', 'ID', 'Type'];
$data = [];
$count = 1;
foreach ($files as $file) {
    $data[] = [$count, $file['name'], $file['id'], $file['mimeType']];
    $count++;
}

$table = new \cli\Table();
$table->setHeaders($headers);
$table->setRows($data);
$table->setRenderer(new \cli\table\Ascii([5, 30, 40, 25]));
$table->display();

$downloadNumber = (int) \cli\prompt('Download file #');

$downloadNumber--;

if (empty($data[$downloadNumber])) {
    \cli\err('Invalid download number');
    exit(1);
}
$fileId = $data[$downloadNumber][2];

print "downloading $fileId\n";
$response = send(createRequest('GET', '/api/fileTransfer/v1/SolDel_FileTransfer/' . $fileId));

$fileData = json_decode($response->getBody()->getContents(), true)['data'];
$fileHandle = fopen('downloads/' . $fileData['id'] . '_' . $fileData['name'], 'wb');
fwrite($fileHandle, base64_decode($fileData['content']));
fclose($fileHandle);

function createRequest(string $method, string $uri, array $headers = [], string $body = null): Request
{
    if (strpos($uri, '?') === false) {
        $uri .= '?';
    } else {
        $uri .= '&';
    }

    $uri .= 'token=' . $GLOBALS['token'];

    return new Request($method, $uri, $headers, $body);
}

function send(Request $request): Response
{
    try {
        return $GLOBALS['client']->send($request);
    } catch (\GuzzleHttp\Exception\ClientException $e) {
        if ($e->getCode() === 401) {
            print "Authentication failed, please provide a valid token\n";
        }
        exit;
    }
}