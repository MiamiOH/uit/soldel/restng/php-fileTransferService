<?php

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7;

require_once __DIR__ . '/../vendor/autoload.php';

$apiBase = 'https://localhost';
$uploadFile = 'classes.xlsx';
$GLOBALS['token'] = 'file-transfer';

if (!file_exists($uploadFile)) {
    print "Upload file $uploadFile not found\n";
    exit(1);
}

$GLOBALS['client'] = new \GuzzleHttp\Client([
    'base_uri' => $apiBase,
    'verify' => false,
]);

$headers = [
    'Content-Type' => 'application/json',
];

$resource = fopen($uploadFile, 'rb');
$contents = fread($resource, filesize($uploadFile));
fclose($resource);
$fileInfo = [
    'name' => basename($uploadFile),
    'content' => base64_encode($contents),
    'mimeType' => mime_content_type($uploadFile),
];

$response = send(createRequest('POST', '/api/fileTransfer/v1/SolDel_FileTransfer', $headers, json_encode($fileInfo)));
$fileUpload = json_decode($response->getBody()->getContents(), true)['data'];

if ($response->getStatusCode() === 201) {
    print "File upload complete\n";
    exit;
}

print "File upload failed.\n";
print_r(json_decode($response->getBody()->getContents(), true));
exit(1);

function createRequest(string $method, string $uri, array $headers = [], string $body = null): Request
{
    if (strpos($uri, '?') === false) {
        $uri .= '?';
    } else {
        $uri .= '&';
    }

    $uri .= 'token=' . $GLOBALS['token'];

    return new Request($method, $uri, $headers, $body);
}

function send(Request $request): Response
{
    try {
        return $GLOBALS['client']->send($request);
    } catch (\GuzzleHttp\Exception\ClientException $e) {
        if ($e->getCode() === 401) {
            print "Authentication failed, please provide a valid token\n";
        }
        exit;
    }
}