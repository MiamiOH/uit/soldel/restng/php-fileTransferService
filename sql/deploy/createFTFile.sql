BEGIN
   EXECUTE IMMEDIATE 'drop table ft_file';
   EXECUTE IMMEDIATE 'drop sequence ft_file_id_seq';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/

create sequence ft_file_id_seq start with 100 increment by 1 minvalue 0 nocache nocycle noorder;

CREATE TABLE ft_file
 (
   id NUMBER(8,0) NOT NULL ENABLE,
   transport varchar2(24) NOT NULL ENABLE,
   destination varchar2(24) NOT NULL ENABLE,
   file_name varchar2(64) NOT NULL ENABLE,
   status varchar2(16) NOT NULL ENABLE,
   error clob
);
