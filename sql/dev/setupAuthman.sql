declare

  v_authman_app_name varchar2(64) := null;
  v_authman_module_name varchar2(64) := null;
  v_authman_entity_name varchar2(64) := null;
  v_authman_grantkey varchar2(64) := null;

  v_application_id number;

begin

  v_authman_app_name := 'File Transfer';
  v_authman_module_name := 'FileCopy';
  v_authman_entity_name := 'testuser';
  v_authman_grantkey := 'testFiles:create';

  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                       v_authman_grantkey, 'doej');

  v_authman_module_name := 'S3';
  v_authman_grantkey := 'testFiles:create';

  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                       v_authman_grantkey, 'doej');

  v_authman_grantkey := 'MentisS3Files:create';

  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                       v_authman_grantkey, 'doej');

  v_authman_module_name := 'GoogleDrive';
  v_authman_grantkey := 'verifytest:create';

  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                       v_authman_grantkey, 'doej');

  commit;

end;
/
