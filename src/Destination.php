<?php


namespace MiamiOH\FileTransferService;


class Destination
{

    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $transport;
    /**
     * @var string
     */
    private $location;
    /**
     * @var string
     */
    private $credential;
    /**
     * @var string|null
     */
    private $secret;

    public function __construct(string $name, string $transport, string $location, string $credential, ?string $secret)
    {
        $this->name = $name;
        $this->transport = $transport;
        $this->location = $location;
        $this->credential = $credential;
        $this->secret = $secret;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function transport(): string
    {
        return $this->transport;
    }

    public function location(): string
    {
        return $this->location;
    }

    public function credential(): string
    {
        return $this->credential;
    }

    public function secret(): ?string
    {
        return $this->secret;
    }
}