<?php

namespace MiamiOH\FileTransferService\Exceptions;

class BadArgumentException extends FileTransferException
{
}
