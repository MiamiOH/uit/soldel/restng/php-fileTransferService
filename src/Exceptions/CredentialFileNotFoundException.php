<?php

namespace MiamiOH\FileTransferService\Exceptions;

class CredentialFileNotFoundException extends FileTransferException
{
}
