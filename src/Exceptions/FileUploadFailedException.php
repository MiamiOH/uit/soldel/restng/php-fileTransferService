<?php

namespace MiamiOH\FileTransferService\Exceptions;

class FileUploadFailedException extends FileTransferException
{
}
