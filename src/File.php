<?php

namespace MiamiOH\FileTransferService;

use MiamiOH\FileTransferService\Exceptions\BadArgumentException;

class File
{

    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $mimeType;
    /**
     * @var string
     */
    private $content;
    /**
     * @var string|null
     */
    private $id;

    public function __construct(string $name, string $mimeType, string $content, string $id = null)
    {
        $this->name = $name;
        $this->mimeType = $mimeType;
        $this->content = $content;
        $this->id = $id;
    }

    /**
     * @param array $data
     * @return File
     * @throws BadArgumentException
     */
    public static function fromArray(array $data): self
    {
        $id = $data['id'] ?? null;

        if (empty($data['name'])) {
            throw new BadArgumentException('File::name cannot be empty');
        }
        $name = $data['name'];

        if (empty($data['mimeType'])) {
            throw new BadArgumentException('File::mimeType cannot be empty');
        }
        $mimeType = $data['mimeType'];

        $content = $data['content'] ?? '';

        return new self($name, $mimeType, $content, $id);
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'mimeType' => $this->mimeType,
            'content' => $this->content,
        ];
    }

    public function name(): string
    {
        return $this->name;
    }

    public function mimeType(): string
    {
        return $this->mimeType;
    }

    public function content(): string
    {
        return $this->content;
    }

    public function setContentRaw(string $content): void
    {
        $this->content = base64_encode($content);
    }

    public function contentRaw(): string
    {
        return base64_decode($this->content);
    }

    public function id(): ?string
    {
        return $this->id;
    }
}