<?php


namespace MiamiOH\FileTransferService;


class FileCollection implements \Countable, \IteratorAggregate
{
    /** @var  File[] */
    private $files;

    public function __construct(array $files)
    {
        $this->ensure($files);
        $this->files = $files;
    }

    private function ensure(array $files): void
    {
        foreach ($files as $file) {
            if( !$file instanceof File) {
                throw new \InvalidArgumentException();
            }
        }
    }

    public function getIterator(): FileCollectionIterator
    {
        return new FileCollectionIterator($this);
    }

    public function count(): int
    {
        return count($this->files);
    }

    public function toArray(): array
    {
        return $this->files;
    }

    public function filesToArray(): array
    {
        $fileArray = [];

        foreach ($this->files as $file) {
            $fileArray[] = $file->toArray();
        }

        return $fileArray;
    }
}