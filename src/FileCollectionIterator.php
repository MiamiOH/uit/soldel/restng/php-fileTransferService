<?php


namespace MiamiOH\FileTransferService;


class FileCollectionIterator implements \Iterator
{
    private $current = 0;

    /**
     * @var File[]
     */
    private $files;

    public function __construct(FileCollection $fileCollection)
    {
        $this->files = $fileCollection->toArray();
    }

    public function current(): File
    {
        return $this->files[$this->current];
    }

    public function next(): void
    {
        $this->current++;
    }

    public function key(): int
    {
        return $this->current;
    }

    public function valid(): bool
    {
        return isset($this->files[$this->current]);
    }

    public function rewind(): void
    {
        $this->current = 0;
    }
}