<?php


namespace MiamiOH\FileTransferService;


use MiamiOH\FileTransferService\Transports\Transport;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Connector\DataSourceFactory;

class Resolver
{
    /** @var App */
    private $app;

    /** @var DataSourceFactory */
    private $dataSourceFactory;

    public function setApp(App $app): void
    {
        $this->app = $app;
    }

    public function setDataSourceFactory(DataSourceFactory $dataSourceFactory): void
    {
        $this->dataSourceFactory = $dataSourceFactory;
    }

    public function resolveDestination(string $name): Destination
    {
        $dataSource = $this->dataSourceFactory->getDataSource($name);

        return new Destination(
            $dataSource->getName(),
            $dataSource->getHost(),
            $dataSource->getDatabase(),
            $dataSource->getUser(),
            $dataSource->getPassword()
        );
    }

    public function resolveTransport(Destination $destination): Transport
    {
        /** @var Transport $transport */
        $transport = $this->app->getService('FileTransfer.' . $destination->transport());

        $transport->withDestination($destination);

        return $transport;
    }
}