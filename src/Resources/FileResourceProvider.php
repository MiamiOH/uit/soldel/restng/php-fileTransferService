<?php

namespace MiamiOH\FileTransferService\Resources;

use MiamiOH\FileTransferService\Transports\GoogleDriveFiles;
use MiamiOH\FileTransferService\Transports\GoogleDriveTransport;
use MiamiOH\FileTransferService\Transports\GoogleServiceFactory;
use MiamiOH\FileTransferService\Transports\S3Transport;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class FileResourceProvider extends ResourceProvider
{
    public function registerDefinitions(): void
    {
        $this->addDefinition([
            'name' => 'FileTransfer.File',
            'type' => 'object',
            'properties' => [
                'id' => [
                    'type' => 'integer',
                ],
                'name' => [
                    'type' => 'string',
                ],
                'mimeType' => [
                    'type' => 'string',
                ],
                'content' => [
                    'type' => 'string',
                ],
            ],
        ]);

        $this->addDefinition([
            'name' => 'FileTransfer.File.Collection',
            'type' => 'array',
            'items' => [
                '$ref' => '#/definitions/FileTransfer.File'
            ]
        ]);
    }

    public function registerServices(): void
    {
        $this->addService([
            'name' => 'FileTransfer.Resolver',
            'class' => \MiamiOH\FileTransferService\Resolver::class,
            'description' => 'Resolves destinations and transports.',
            'set' => [
                'dataSourceFactory' => ['type' => 'service', 'name' => 'APIDataSourceFactory'],
                'app' => ['type' => 'service', 'name' => 'APIApp'],
            ],
        ]);

        $this->addService([
            'name' => 'FileTransfer.FileStore',
            'class' => \MiamiOH\FileTransferService\Services\FileStore::class,
            'description' => 'Implements persistent storage for file transfers.',
            'set' => [
                'resolver' => ['type' => 'service', 'name' => 'FileTransfer.Resolver'],
            ],
        ]);

        $this->addService([
            'name' => 'FileTransfer.FileStoreREST',
            'class' => \MiamiOH\FileTransferService\Services\FileStoreRest::class,
            'description' => 'Provides REST resource definitions for file transfers',
            'set' => [
                'fileStore' => ['type' => 'service', 'name' => 'FileTransfer.FileStore'],
            ],
        ]);

        $this->addService([
            'name' => 'FileTransfer.GoogleDrive',
            'description' => 'Implements Google Drive upload.',
            'object' => new GoogleDriveTransport(new GoogleDriveFiles(new GoogleServiceFactory())),
        ]);

        $this->addService(array(
            'name' => 'FileTransfer.S3',
            'class' => S3Transport::class,
            'description' => 'Implements AWS S3 upload.',
            'set' => array(
                'dataSourceFactory' => array('type' => 'service', 'name' => 'APIDataSourceFactory'),
            ),
        ));
    }

    public function registerResources(): void
    {
        $this->addResource([
            'action' => 'read',
            'name' => 'fileTransfer.v1.read.list',
            'description' => 'Get the list of available files',
            'tags' => ['File Transfer'],
            'pattern' => '/fileTransfer/v1/:destination',
            'service' => 'FileTransfer.FileStoreREST',
            'method' => 'listFiles',
            'params' => [
                'destination' => ['description' => 'The destination to use'],
            ],

            'middleware' => [
                'authenticate' => [],
            ],

            'responses' => [
                App::API_OK => [
                    'description' => 'A collection of file objects',
                    'returns' => [
                        'type' => 'array',
                        '$ref' => '#/definitions/FileTransfer.File.Collection',
                    ]
                ]
            ]
        ]);

        $this->addResource([
            'action' => 'read',
            'name' => 'fileTransfer.v1.read.id',
            'description' => 'Get a specific file from the destination',
            'tags' => ['File Transfer'],
            'pattern' => '/fileTransfer/v1/:destination/:id',
            'service' => 'FileTransfer.FileStoreREST',
            'method' => 'getFile',
            'params' => [
                'destination' => ['description' => 'The destination target to use'],
                'id' => ['description' => 'The id of the file to download'],
            ],

            'middleware' => [
                'authenticate' => [],
            ],

            'responses' => [
                App::API_OK => [
                    'description' => 'A file object',
                    'returns' => [
                        'type' => 'model',
                        '$ref' => '#/definitions/FileTransfer.File',
                    ]
                ]
            ]
        ]);

        $this->addResource([
            'action' => 'create',
            'name' => 'fileTransfer.v1.create',
            'description' => 'Create a new file object',
            'tags' => ['File Transfer'],
            'pattern' => '/fileTransfer/v1/:destination',
            'service' => 'FileTransfer.FileStoreREST',
            'method' => 'createFile',
            'params' => [
                'destination' => ['description' => 'The destination target to use'],
            ],

            'middleware' => [
                'authenticate' => [],
            ],

            'body' => [
                'description' => 'File object',
                'required' => true,
                'schema' => [
                    '$ref' => '#/definitions/FileTransfer.File'
                ]
            ],

            'responses' => [
                App::API_OK => [
                    'description' => 'The upload was successful',
                    'returns' => [
                        'type' => 'model',
                        '$ref' => '#/definitions/FileTransfer.File',
                    ]
                ],
            ]
        ]);

        $this->addResource([
            'action' => 'delete',
            'name' => 'fileTransfer.v1.delete.id',
            'description' => 'Get a specific file from the destination',
            'tags' => ['File Transfer'],
            'pattern' => '/fileTransfer/v1/:destination/:id',
            'service' => 'FileTransfer.FileStoreREST',
            'method' => 'deleteFile',
            'params' => [
                'destination' => ['description' => 'The destination target to use'],
                'id' => ['description' => 'The id of the file to download'],
            ],

            'middleware' => [
                'authenticate' => [],
            ],

            'responses' => [
                App::API_OK => [
                    'description' => 'A success response',
                ]
            ]
        ]);
    }

    public function registerOrmConnections(): void
    {
    }
}
