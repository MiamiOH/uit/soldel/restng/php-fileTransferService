<?php

namespace MiamiOH\FileTransferService\Services;


use MiamiOH\FileTransferService\File;
use MiamiOH\FileTransferService\FileCollection;
use MiamiOH\FileTransferService\Resolver;
use MiamiOH\FileTransferService\Transports\Transport;
use MiamiOH\RESTng\Exception\UnAuthorized;

class FileStore extends \MiamiOH\RESTng\Service
{
    public const AUTHMAN_APP_NAME = 'File Transfer';
    public const LIST_ACTION = 'list';
    public const GET_ACTION = 'get';
    public const CREATE_ACTION = 'create';
    public const DELETE_ACTION = 'delete';

    /** @var Resolver */
    private $resolver;

    public function setResolver(Resolver $resolver): void
    {
        $this->resolver = $resolver;
    }

    public function getFileList(string $destinationName): FileCollection
    {
        $this->authorize($destinationName, self::LIST_ACTION);

        $transport = $this->resolveTransport($destinationName);

        return $transport->listFiles();
    }

    public function getFile(string $destinationName, string $id): File
    {
        $this->authorize($destinationName, self::GET_ACTION);

        $transport = $this->resolveTransport($destinationName);

        return $transport->getFile($id);
    }

    public function createFile(string $destinationName, File $file): File
    {
        $this->authorize($destinationName, self::CREATE_ACTION);

        $transport = $this->resolveTransport($destinationName);

        return $transport->createFile($file);
    }

    public function deleteFile(string $destinationName, string $id): void
    {
        $this->authorize($destinationName, self::DELETE_ACTION);

        $transport = $this->resolveTransport($destinationName);

        $transport->deleteFile($id);
    }

    private function resolveTransport(string $destinationName): Transport
    {
        $destination = $this->resolver->resolveDestination($destinationName);

        return $this->resolver->resolveTransport($destination);
    }

    private function authorize(string $destinationName, string $action): void
    {
        $user = $this->getApiUser();

        $authConfig = [
            'type' => 'authMan',
            'application' => self::AUTHMAN_APP_NAME,
            'module' => $destinationName,
            'key' => $action
        ];

        if ($user->checkAuthorization($authConfig)) {
            return;
        }

        throw new UnAuthorized($user->getUsername() . ' is not authorized to ' . $action . ' with ' . $destinationName);
    }
}
