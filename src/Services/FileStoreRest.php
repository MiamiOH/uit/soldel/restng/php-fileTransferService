<?php

namespace MiamiOH\FileTransferService\Services;

use MiamiOH\FileTransferService\File;
use MiamiOH\RESTng\Exception\UnAuthorized;
use MiamiOH\RESTng\Util\Response;

class FileStoreRest extends \MiamiOH\RESTng\Service
{
    /** @var FileStore $fileStore */
    private $fileStore;

    public function setFileStore(FileStore $fileStore): void
    {
        $this->fileStore = $fileStore;
    }

    public function listFiles(): Response
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $destination = $request->getResourceParam('destination');

        $fileList = $this->fileStore->getFileList($destination);

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($fileList->filesToArray());

        return $response;
    }

    public function getFile(): Response
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $destination = $request->getResourceParam('destination');
        $id = $request->getResourceParam('id');

        $file = $this->fileStore->getFile($destination, $id);

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($file->toArray());

        return $response;
    }

    public function createFile(): Response
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $destination = $request->getResourceParam('destination');

        $newFile = File::fromArray($request->getData());

        $file = $this->fileStore->createFile($destination, $newFile);

        $response->setStatus(\MiamiOH\RESTng\App::API_CREATED);
        $response->setPayload($file->toArray());

        return $response;
    }

    public function deleteFile(): Response
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $destination = $request->getResourceParam('destination');
        $id = $request->getResourceParam('id');

        $this->fileStore->deleteFile($destination, $id);

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);

        return $response;
    }
}
