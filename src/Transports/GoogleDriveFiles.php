<?php


namespace MiamiOH\FileTransferService\Transports;


use MiamiOH\FileTransferService\Destination;
use MiamiOH\FileTransferService\File;
use MiamiOH\FileTransferService\FileCollection;

class GoogleDriveFiles
{
    public const MAX_RESULT_LIST_SIZE = 100;

    /** @var GoogleServiceFactory */
    private $factory;
    /** @var Destination */
    private $destination;
    /** @var \Google_Service_Drive_Resource_Files */
    private $files;

    public function __construct(GoogleServiceFactory $factory = null)
    {
        if (null === $factory) {
            $factory = new GoogleServiceFactory();
        }

        $this->factory = $factory;
    }

    public function withDestination(Destination $destination): self
    {
        $this->destination = $destination;
        $this->initialize();

        return $this;
    }

    public function listFiles(): FileCollection
    {
        // Refer to example on https://developers.google.com/drive/api/v3/search-parameters
        $optParams = [
            'pageSize' => self::MAX_RESULT_LIST_SIZE,
            'fields' => 'nextPageToken, files(id, name, mimeType)',
            'spaces' => 'drive',
            'corpora' => 'user',
            'supportsTeamDrives' => 'true',
            'includeTeamDriveItems' => 'true',
            'q' => sprintf("'%s' in parents", $this->destination->location()),
        ];
        $fileList = $this->files->listFiles($optParams);

        $files = [];

        /** @var \Google_Service_Drive_DriveFile $file */
        foreach ($fileList->getFiles() as $file) {
            $files[] = File::fromArray([
                'id' => $file->getId(),
                'name' => $file->getName(),
                'mimeType' => $file->getMimeType(),
            ]);
        }

        return new FileCollection($files);
    }

    public function getFile(string $id): File
    {
        $optParams = [
            'supportsTeamDrives' => 'true',
        ];
        $driveFile = $this->files->get($id, $optParams);
        $optParams['alt'] = 'media';
        $fileContent = $this->files->get($id, $optParams);

        $file = File::fromArray([
            'id' => $driveFile->getId(),
            'name' => $driveFile->getName(),
            'mimeType' => $driveFile->getMimeType(),
        ]);

        $file->setContentRaw($fileContent->getBody()->getContents());

        return $file;
    }

    public function createFile(File $file): File
    {
        $newFile = $this->factory->createDriveFile();
        $newFile->setParents([$this->destination->location()]);
        $newFile->setName($file->name());

        $optParams = [
            'data' => $file->contentRaw(),
            'mimeType' => 'application/octet-stream',
            'uploadType' => 'multipart',
            'supportsTeamDrives' => 'true',
        ];
        try {
            $createdFile = $this->files->create($newFile, $optParams);
        } catch (\Exception $e) {
            throw new \MiamiOH\FileTransferService\Exceptions\FileUploadFailedException($e->getMessage());
        }

        return File::fromArray([
            'id' => $createdFile->getId(),
            'name' => $createdFile->getName(),
            'mimeType' => $createdFile->getMimeType(),
        ]);
    }

    public function deleteFile(string $id): void
    {
        $optParams = [
            'supportsTeamDrives' => 'true',
        ];
        $this->files->delete($id, $optParams);
    }

    private function initialize(): void
    {
        $this->factory->setCredentials($this->destination->credential());

        $client = $this->factory->createClient();
        $client->useApplicationDefaultCredentials();

        $client->addScope([
            \Google_Service_Drive::DRIVE,
        ]);

        $this->files = $this->factory->createDriveFileResource($client);
    }
}

// for future reference
//   Create new folder as the service account
//        $folderMetaData = $this->factory->createDriveFile(
//            [
//                'name' => 'FileUploads',
//                'mimeType' => 'application/vnd.google-apps.folder'
//            ]
//        );
//
//        $folder = $this->files->create($folderMetaData, ['fields' => 'id']);
//
//        print "id: " . $folder->id . "\n";exit;
//
//    Invite other users to share the folder
//        $client = $this->factory->createClient();
//
//        $fileId = '...';
//        $this->driveService->getClient()->setUseBatch(true);
//        try {
//            $batch = $this->driveService->createBatch();
//
//            $userPermission = new \Google_Service_Drive_Permission(array(
//                'type' => 'user',
//                'role' => 'writer',
//                'emailAddress' => 'tepeds@miamioh.edu'
//            ));
//            $request = $this->driveService->permissions->create(
//                $fileId, $userPermission, array('fields' => 'id'));
//            $batch->add($request, 'user');
//            $results = $batch->execute();
//
//            foreach ($results as $result) {
//                if ($result instanceof \Google_Service_Exception) {
//                    // Handle error
//                    printf($result);
//                } else {
//                    printf("Permission ID: %s\n", $result->id);
//                }
//            }
//        } finally {
//            $this->driveService->getClient()->setUseBatch(false);
//        }
