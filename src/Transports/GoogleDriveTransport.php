<?php

namespace MiamiOH\FileTransferService\Transports;

use MiamiOH\FileTransferService\Destination;
use MiamiOH\FileTransferService\File;
use MiamiOH\FileTransferService\FileCollection;

class GoogleDriveTransport implements Transport
{
    /** @var Destination */
    private $destination;
    /** @var GoogleDriveFiles */
    private $driveFiles;

    public function __construct(GoogleDriveFiles $driveFiles = null)
    {
        if (null === $driveFiles) {
            $driveFiles = new GoogleDriveFiles();
        }
        $this->driveFiles = $driveFiles;
    }

    public function withDestination(Destination $destination): Transport
    {
        $this->destination = $destination;

        $this->driveFiles->withDestination($destination);

        return $this;
    }

    public function listFiles(): FileCollection
    {
        return $this->driveFiles->listFiles();
    }

    public function getFile(string $fileId): File
    {
        return $this->driveFiles->getFile($fileId);
    }

    public function createFile(File $file): File
    {
        return $this->driveFiles->createFile($file);
    }

    public function deleteFile(string $fileId): void
    {
        $this->driveFiles->deleteFile($fileId);
    }
}
