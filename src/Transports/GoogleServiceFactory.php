<?php


namespace MiamiOH\FileTransferService\Transports;


use MiamiOH\FileTransferService\Exceptions\CredentialFileNotFoundException;

class GoogleServiceFactory
{
    /**
     * @param string $credentialFile
     * @throws CredentialFileNotFoundException
     */
    public function setCredentials(string $credentialFile): void
    {
        $filePath = env('CREDENTIAL_PATH') . '/' . $credentialFile;

        if (!file_exists($filePath)) {
            throw new CredentialFileNotFoundException(sprintf('The Google API credential file "%s" was not found', $credentialFile));
        }

        putenv('GOOGLE_APPLICATION_CREDENTIALS=' . $filePath);
    }

    public function createClient(): \Google_Client
    {
        return new \Google_Client;
    }

    public function createDriveService(\Google_Client $client): \Google_Service_Drive
    {
        return new \Google_Service_Drive($client);
    }

    public function createDriveFileResource(\Google_Client $client): \Google_Service_Drive_Resource_Files
    {
        return $this->createDriveService($client)->files;
    }

    public function createDriveFile(array $options = []): \Google_Service_Drive_DriveFile
    {
        return new \Google_Service_Drive_DriveFile($options);
    }
}