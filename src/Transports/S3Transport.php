<?php

namespace MiamiOH\FileTransferService\Transports;


use MiamiOH\FileTransferService\Destination;
use MiamiOH\FileTransferService\File;
use MiamiOH\FileTransferService\FileCollection;

class S3Transport implements Transport
{
    private $dsFactory;

    /** @var Destination */
    private $destination;

    public function setDataSourceFactory($dsFactory)
    {
        $this->dsFactory = $dsFactory;
    }

    public function withDestination(Destination $destination): Transport
    {
        $this->destination = $destination;

        return $this;
    }

    public function listFiles(): FileCollection
    {
        // TODO: Implement listFiles() method.
    }

    public function getFile(string $fileId): File
    {
        // TODO: Implement getFile() method.
    }

    public function createFile(File $file): File
    {
        putenv('AWS_ACCESS_KEY_ID=' . $this->destination->credential());
        putenv('AWS_SECRET_ACCESS_KEY=' . $this->destination->secret());

        [$region, $bucket] = explode(':', $this->destination->location());

        $s3Client = new \Aws\S3\S3Client([
            'version' => 'latest',
            'region' => $region,
        ]);

        $tmpFile = '/tmp/' . uniqid('ft_');

        try {
            file_put_contents($tmpFile, $file->contentRaw());
        } catch (\Exception $e) {
            throw new \MiamiOH\FileTransferService\Exceptions\FileUploadFailedException(
                'Failed to create temp file for upload',
                $e->getCode(),
                $e
            );
        }

        try {
            $result = $s3Client->putObject([
                'Bucket' => $bucket,
                'Key' => $file->name(),
                'SourceFile' => $tmpFile,
            ]);
        } catch (\Aws\S3\Exception\S3Exception $e) {
            // Catch an S3 specific exception.
            throw new \MiamiOH\FileTransferService\Exceptions\FileUploadFailedException($e->getMessage());
        } catch (\Aws\Exception\AwsException $e) {
            // This catches the more generic AwsException. You can grab information
            // from the exception using methods of the exception object.
            throw new \MiamiOH\FileTransferService\Exceptions\FileUploadFailedException(
                $e->getAwsErrorType() . "\n" .
                $e->getAwsErrorCode()
            );
        }

        return $file;
    }

    public function deleteFile(string $fileId): void
    {
        // TODO: Implement deleteFile() method.
    }
}
