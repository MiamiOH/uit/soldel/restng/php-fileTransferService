<?php

namespace MiamiOH\FileTransferService\Transports;

use MiamiOH\FileTransferService\Destination;
use MiamiOH\FileTransferService\File;
use MiamiOH\FileTransferService\FileCollection;

interface Transport
{
    public function withDestination(Destination $destination): self;

    public function listFiles(): FileCollection;
    public function getFile(string $fileId): File;
    public function createFile(File $file): File;
    public function deleteFile(string $fileId): void;
}
