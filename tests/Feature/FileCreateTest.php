<?php


namespace MiamiOH\FileTransferService\Tests\Feature;


use MiamiOH\FileTransferService\File;
use MiamiOH\FileTransferService\Tests\FeatureTestCase;
use MiamiOH\RESTng\Exception\UnAuthorized;

class FileCreateTest extends FeatureTestCase
{
    public function testRequiresAuthorizationToGetListOfFilesFromDestination(): void
    {
        $destination = 'test-destination';
        $file = File::fromArray($this->makeFileModel());

        $this->fileStore->expects($this->once())
            ->method('createFile')
            ->with($this->equalTo($destination))
            ->will($this->throwException(new UnAuthorized));

        $response = $this->postJson($this->addAllowedToken('/fileTransfer/v1/' . $destination), $file->toArray());

        $response->assertStatus(401);
    }

    public function testCanPostFileToDestination(): void
    {
        $destination = 'test-destination';
        $file = File::fromArray($this->makeFileModel());
        $newFile = File::fromArray($expected = [
            'id' => 123,
            'name' => $file->name(),
            'mimeType' => $file->mimeType(),
            'content' => $file->content(),
        ]);

        $this->fileStore->expects($this->once())
            ->method('createFile')
            ->with($this->equalTo($destination), $this->equalTo($file))
            ->willReturn($newFile);

        $response = $this->postJson($this->addAllowedToken('/fileTransfer/v1/' . $destination), $file->toArray());

        $response->assertStatus(201)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'name',
                    'mimeType',
                    'content',
                ]
            ]);
    }
}