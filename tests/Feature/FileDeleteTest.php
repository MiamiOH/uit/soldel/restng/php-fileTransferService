<?php


namespace MiamiOH\FileTransferService\Tests\Feature;


use MiamiOH\FileTransferService\Tests\FeatureTestCase;
use MiamiOH\RESTng\Exception\UnAuthorized;

class FileDeleteTest extends FeatureTestCase
{
    public function testRequiresAuthorizationToGetDeleteFileFromDestination(): void
    {
        $destination = 'test-destination';
        $id = 'abc123';

        $this->fileStore->expects($this->once())
            ->method('deleteFile')
            ->with($this->equalTo($destination))
            ->will($this->throwException(new UnAuthorized));

        $response = $this->deleteJson($this->addAllowedToken('/fileTransfer/v1/' . $destination . '/' . $id));

        $response->assertStatus(401);
    }

    public function testCanDeleteFileFromDestination(): void
    {
        $destination = 'test-destination';
        $id = 'abc123';

        $this->fileStore->expects($this->once())
            ->method('deleteFile')
            ->with($this->equalTo($destination), $this->equalTo($id));

        $response = $this->deleteJson($this->addAllowedToken('/fileTransfer/v1/' . $destination . '/' . $id));

        $response->assertStatus(200);
    }

}