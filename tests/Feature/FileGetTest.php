<?php


namespace MiamiOH\FileTransferService\Tests\Feature;


use MiamiOH\FileTransferService\File;
use MiamiOH\FileTransferService\Tests\FeatureTestCase;
use MiamiOH\RESTng\Exception\UnAuthorized;

class FileGetTest extends FeatureTestCase
{
    public function testRequiresAuthorizationToDownloadFileFromDestination(): void
    {
        $destination = 'test-destination';
        $id = 'abc123';

        $this->fileStore->expects($this->once())
            ->method('getFile')
            ->with($this->equalTo($destination))
            ->will($this->throwException(new UnAuthorized));

        $response = $this->getJson($this->addAllowedToken('/fileTransfer/v1/' . $destination . '/' . $id));

        $response->assertStatus(401);
    }

    public function testCanDownloadFileFromDestination(): void
    {
        $destination = 'test-destination';
        $id = 'abc123';
        $file = File::fromArray($this->makeFileModel(['id' => $id]));

        $this->fileStore->expects($this->once())
            ->method('getFile')
            ->with($this->equalTo($destination), $this->equalTo($id))
            ->willReturn($file);

        $response = $this->getJson($this->addAllowedToken('/fileTransfer/v1/' . $destination . '/' . $id));

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'name',
                    'mimeType',
                    'content',
                ]
            ]);
    }
}