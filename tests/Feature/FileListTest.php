<?php


namespace MiamiOH\FileTransferService\Tests\Feature;


use MiamiOH\FileTransferService\File;
use MiamiOH\FileTransferService\FileCollection;
use MiamiOH\FileTransferService\Tests\FeatureTestCase;
use MiamiOH\RESTng\Exception\UnAuthorized;

class FileListTest extends FeatureTestCase
{
    public function testRequiresAuthorizationToGetListOfFilesFromDestination(): void
    {
        $destination = 'test-destination';

        $this->fileStore->expects($this->once())
            ->method('getFileList')
            ->with($this->equalTo($destination))
            ->will($this->throwException(new UnAuthorized));

        $response = $this->getJson($this->addAllowedToken('/fileTransfer/v1/' . $destination));

        $response->assertStatus(401);
    }

    public function testCanGetListOfFilesFromDestination(): void
    {
        $destination = 'test-destination';
        $files = new FileCollection([
            File::fromArray($this->makeFileModel(['id' => 123])),
        ]);

        $this->fileStore->expects($this->once())
            ->method('getFileList')
            ->with($this->equalTo($destination))
            ->willReturn($files);

        $response = $this->getJson($this->addAllowedToken('/fileTransfer/v1/' . $destination));

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    [
                        'id',
                        'name',
                        'mimeType',
                        'content',
                    ]
                ]
            ]);
    }
}