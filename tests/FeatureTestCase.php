<?php

namespace MiamiOH\FileTransferService\Tests;

use MiamiOH\FileTransferService\Services\FileStore;
use MiamiOH\RESTng\Testing\UsesAuthentication;
use MiamiOH\RESTng\Testing\UsesAuthorization;
use PHPUnit\Framework\MockObject\MockObject;

abstract class FeatureTestCase extends TestCase
{
    use UsesAuthentication;
    use UsesAuthorization;

    /** @var FileStore|MockObject */
    protected $fileStore;

    protected $allowedToken = 'abc123';

    public function setUp(): void
    {
        parent::setUp();

        $this->fileStore = $this->createMock(FileStore::class);

        $this->useService([
            'name' => 'FileTransfer.FileStore',
            'object' => $this->fileStore,
            'description' => 'FileStore service',
        ]);

        // Authenticate and Authorize user for all test cases (In the cases
        // Where we want the user to not be, it will be changed later)
        $this->withToken($this->allowedToken)
            ->withoutAcceptHeader()
            ->willAuthenticateUser();

        $this->willAuthorizeUser();

    }

    protected function addAllowedToken(string $url): string
    {
        if (strpos($url, '?') === false) {
            $url .= '?';
        } else {
            $url .= '&';
        }

        $url .= 'token=' . $this->allowedToken;

        return $url;
    }
}