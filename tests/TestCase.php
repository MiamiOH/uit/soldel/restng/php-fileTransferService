<?php

namespace MiamiOH\FileTransferService\Tests;

use MiamiOH\RESTng\Connector\DataSource;

abstract class TestCase extends \MiamiOH\RESTng\Testing\TestCase
{
    protected function makeDataSource(array $overrides = []): DataSource
    {
        $defaults = [
            'name' => 'Data Source',
            'type' => 'Other',
            'user' => 'bob',
            'password' => 'sekret',
            'host' => 'example.com',
            'database' => 'main',
        ];

        return DataSource::fromArray(array_merge($defaults, $overrides));
    }

    protected function makeFileModel(array $overrides = []): array
    {
        $defaults = [
            'name' => 'Test file',
            'mimeType' => 'text/plain',
            'content' => base64_encode('test content'),
        ];

        return array_merge($defaults, $overrides);
    }
}