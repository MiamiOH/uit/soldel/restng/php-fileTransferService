<?php

namespace MiamiOH\FileTransferService\Tests\Unit;

use MiamiOH\FileTransferService\Destination;
use PHPUnit\Framework\TestCase;

class DestinationTest extends TestCase
{
    /** @var Destination */
    private $destination;

    public function setUp(): void
    {
        parent::setUp();

        $this->destination = new Destination('test_destination', 'test_transport', 'home', 'bob', 'sekret');
    }

    public function testCanReturnName(): void
    {
        $this->assertEquals('test_destination', $this->destination->name());
    }

    public function testCanReturnTransport(): void
    {
        $this->assertEquals('test_transport', $this->destination->transport());
    }

    public function testCanReturnLocation(): void
    {
        $this->assertEquals('home', $this->destination->location());
    }

    public function testCanReturnUserCredential(): void
    {
        $this->assertEquals('bob', $this->destination->credential());
    }

    public function testCanReturnUserSecret(): void
    {
        $this->assertEquals('sekret', $this->destination->secret());
    }

    public function testUserSecretCanBeNull(): void
    {
        $this->destination = new Destination('test_destination', 'test_transport', 'home', 'bob', null);

        $this->assertNull($this->destination->secret());
    }
}
