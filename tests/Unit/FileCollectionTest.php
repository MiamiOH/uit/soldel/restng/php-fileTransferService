<?php

namespace MiamiOH\FileTransferService\Tests\Unit;

use MiamiOH\FileTransferService\File;
use MiamiOH\FileTransferService\FileCollection;
use MiamiOH\FileTransferService\Tests\TestCase;

class FileCollectionTest extends TestCase
{
    /** @var  FileCollection */
    private $collection;

    public function setUp(): void
    {
        parent::setUp();

        $this->collection = new FileCollection([
            File::fromArray($this->makeFileModel())
        ]);
    }

    public function testRequiresFileObjectsToBeCreated(): void
    {
        $someArray = [new \stdClass(), new \stdClass()];

        $this->expectException(\InvalidArgumentException::class);

        new FileCollection($someArray);
    }

    public function testCanBeCounted(): void
    {
        $this->assertCount(1, $this->collection);
    }

    public function testCanBeUsedAsArray(): void
    {
        foreach ($this->collection->toArray() as $item) {
            $this->assertInstanceOf(File::class, $item);
        }
    }

    public function testCanBeIterated(): void
    {
        foreach ($this->collection as $key => $item) {
            $this->assertInstanceOf(File::class, $item);
        }
    }
}
