<?php

namespace Unit;

use MiamiOH\FileTransferService\Destination;
use MiamiOH\FileTransferService\File;
use MiamiOH\FileTransferService\FileCollection;
use MiamiOH\FileTransferService\Resolver;
use MiamiOH\FileTransferService\Services\FileStore;
use MiamiOH\FileTransferService\Tests\TestCase;
use MiamiOH\FileTransferService\Transports\Transport;
use MiamiOH\RESTng\Exception\UnAuthorized;
use MiamiOH\RESTng\Util\User;
use PHPUnit\Framework\MockObject\MockObject;

class FileStoreTest extends TestCase
{
    /** @var FileStore */
    private $fileStore;

    /** @var Resolver|MockObject */
    private $resolver;
    /** @var Destination */
    private $destination;
    /** @var Transport|MockObject */
    private $transport;
    /** @var User|MockObject */
    private $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->resolver = $this->createMock(Resolver::class);
        $this->destination = new Destination('test_destination', 'test_transport', 'home', 'bob', 'sekret');
        $this->transport = $this->createMock(Transport::class);
        $this->user = $this->createMock(User::class);

        $this->fileStore = new FileStore();
        $this->fileStore->setResolver($this->resolver);
        $this->fileStore->setApiUser($this->user);
    }

    public function testReturnsFileListFromTransport(): void
    {
        $this->grantAuthorization(FileStore::LIST_ACTION);
        $this->resolvesTransport();

        $collection = new FileCollection([
            File::fromArray($this->makeFileModel())
        ]);

        $this->transport->expects($this->once())
            ->method('listFiles')
            ->willReturn($collection);

        $files = $this->fileStore->getFileList($this->destination->name());

        $this->assertSame($collection, $files);
    }

    public function testReturnsFileFromTransport(): void
    {
        $this->grantAuthorization(FileStore::GET_ACTION);
        $this->resolvesTransport();

        $file = File::fromArray($this->makeFileModel());

        $this->transport->expects($this->once())
            ->method('getFile')
            ->with($this->equalTo('abc123'))
            ->willReturn($file);

        $resultFile = $this->fileStore->getFile($this->destination->name(), 'abc123');

        $this->assertSame($file, $resultFile);
    }

    public function testCreatesFileThroughTransport(): void
    {
        $this->grantAuthorization(FileStore::CREATE_ACTION);
        $this->resolvesTransport();

        $newFileModel = $this->makeFileModel();
        $fileIn = File::fromArray($newFileModel);
        $file = File::fromArray(array_merge(['id' => 'abc123'], $newFileModel));

        $this->transport->expects($this->once())
            ->method('createFile')
            ->with($this->equalTo($fileIn))
            ->willReturn($file);

        $resultFile = $this->fileStore->createFile($this->destination->name(), $fileIn);

        $this->assertSame($file, $resultFile);
    }

    public function testDeletesFileThroughTransport(): void
    {
        $this->grantAuthorization(FileStore::DELETE_ACTION);
        $this->resolvesTransport();

        $this->transport->expects($this->once())
            ->method('deleteFile')
            ->with($this->equalTo('abc123'));

        $this->fileStore->deleteFile($this->destination->name(), 'abc123');
    }

    public function testThrowsExceptionIfNotAuthorizedToGetFileList(): void
    {
        $this->denyAuthorization(FileStore::LIST_ACTION);

        // expect exception to be thrown
        $this->expectException(UnAuthorized::class);

        $this->transport->expects($this->never())
            ->method('listFiles');

        $this->fileStore->getFileList($this->destination->name());
    }

    public function testThrowsExceptionIfNotAuthorizedToGetFile(): void
    {
        $this->denyAuthorization(FileStore::GET_ACTION);

        // expect exception to be thrown
        $this->expectException(UnAuthorized::class);

        $this->transport->expects($this->never())
            ->method('getFile');

        $this->fileStore->getFile($this->destination->name(), 'abc123');
    }

    public function testThrowsExceptionIfNotAuthorizedToCreateFile(): void
    {
        $this->denyAuthorization(FileStore::CREATE_ACTION);

        // expect exception to be thrown
        $this->expectException(UnAuthorized::class);

        $this->transport->expects($this->never())
            ->method('createFile');

        $fileIn = File::fromArray($this->makeFileModel());

        $this->fileStore->createFile($this->destination->name(), $fileIn);
    }

    public function testThrowsExceptionIfNotAuthorizedToDeleteFile(): void
    {
        $this->denyAuthorization(FileStore::DELETE_ACTION);

        // expect exception to be thrown
        $this->expectException(UnAuthorized::class);

        $this->transport->expects($this->never())
            ->method('deleteFile');

        $this->fileStore->deleteFile($this->destination->name(), 'abc123');
    }

    private function resolvesTransport(): void
    {
        $this->resolver->expects($this->once())
            ->method('resolveDestination')
            ->with($this->equalTo($this->destination->name()))
            ->willReturn($this->destination);

        $this->resolver->expects($this->once())
            ->method('resolveTransport')
            ->with($this->equalTo($this->destination))
            ->willReturn($this->transport);
    }

    private function grantAuthorization(string $action): void
    {
        $this->user->expects($this->once())
            ->method('checkAuthorization')
            ->with(
                $this->callback(function (array $authConfig) use ($action) {
                    $this->assertEquals('authMan', $authConfig['type']);
                    $this->assertEquals(FileStore::AUTHMAN_APP_NAME, $authConfig['application']);
                    $this->assertEquals($this->destination->name(), $authConfig['module']);
                    $this->assertEquals($action, $authConfig['key']);
                    return true;
                })
            )
            ->willReturn(true);
    }

    private function denyAuthorization(string $action): void
    {
        $this->user->expects($this->once())
            ->method('checkAuthorization')
            ->with(
                $this->callback(function (array $authConfig) use ($action) {
                    $this->assertEquals(FileStore::AUTHMAN_APP_NAME, $authConfig['application']);
                    $this->assertEquals($this->destination->name(), $authConfig['module']);
                    $this->assertEquals($action, $authConfig['key']);
                    return true;
                })
            )
            ->willReturn(false);
    }
}
