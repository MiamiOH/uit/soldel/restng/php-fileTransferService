<?php

namespace MiamiOH\FileTransferService\Tests\Unit;


use MiamiOH\FileTransferService\File;
use MiamiOH\FileTransferService\Tests\TestCase;

class FileTest extends TestCase
{
    public function testCanBeCreatedFromArray(): void
    {
        $data = $this->makeFileModel();

        $file = File::fromArray($data);

        $this->assertInstanceOf(File::class, $file);
    }

    public function testCanBeConvertedToArray(): void
    {
        $data = $this->makeFileModel([
            'id' => 123,
            'name' => 'Test File',
            'mimeType' => 'text/html',
            'content' => base64_encode('Test content'),
        ]);

        $file = File::fromArray($data);

        $arrayData = $file->toArray();

        $this->assertTrue(is_array($arrayData));
        $this->assertEquals($data, $arrayData);
    }

    public function testCanReturnId(): void
    {
        $data = $this->makeFileModel(['id' => 123]);

        $file = File::fromArray($data);

        $this->assertEquals(123, $file->id());
    }

    public function testIdCanBeNull(): void
    {
        $data = $this->makeFileModel();

        $file = File::fromArray($data);

        $this->assertNull($file->id());
    }

    public function testCanReturnName(): void
    {
        $data = $this->makeFileModel(['name' => 'Test File']);

        $file = File::fromArray($data);

        $this->assertEquals('Test File', $file->name());
    }

    public function testCanReturnMimeType(): void
    {
        $data = $this->makeFileModel(['mimeType' => 'text/html']);

        $file = File::fromArray($data);

        $this->assertEquals('text/html', $file->mimeType());
    }

    public function testCanReturnContent(): void
    {
        $fileContent = base64_encode('Test content');
        $data = $this->makeFileModel(['content' => $fileContent]);

        $file = File::fromArray($data);

        $this->assertEquals($fileContent, $file->content());
    }

    public function testCanSetRawContent(): void
    {
        $fileContent = 'Test content';
        $data = $this->makeFileModel();

        $file = File::fromArray($data);
        $file->setContentRaw($fileContent);

        $this->assertEquals($fileContent, base64_decode($file->content()));
    }

    public function testCanReturnRawContent(): void
    {
        $fileContent = base64_encode('Test content');
        $data = $this->makeFileModel(['content' => $fileContent]);

        $file = File::fromArray($data);

        $this->assertEquals(base64_decode($fileContent), $file->contentRaw());
    }
}
