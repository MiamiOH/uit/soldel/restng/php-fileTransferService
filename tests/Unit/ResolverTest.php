<?php

namespace MiamiOH\FileTransferService\Tests\Unit;

use MiamiOH\FileTransferService\Destination;
use MiamiOH\FileTransferService\Resolver;
use MiamiOH\FileTransferService\Tests\TestCase;
use MiamiOH\FileTransferService\Transports\Transport;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Connector\DataSourceFactory;
use PHPUnit\Framework\MockObject\MockObject;

class ResolverTest extends TestCase
{
    /** @var Resolver */
    private $resolver;

    /** @var DataSourceFactory|MockObject */
    private $dataSourceFactory;
    /** @var App|MockObject */
    private $restngApp;

    public function setUp(): void
    {
        parent::setUp();

        $this->restngApp = $this->createMock(App::class);
        $this->dataSourceFactory = $this->createMock(DataSourceFactory::class);

        $this->resolver = new Resolver();
        $this->resolver->setApp($this->restngApp);
        $this->resolver->setDataSourceFactory($this->dataSourceFactory);
    }

    public function testCanResolveDestination(): void
    {
        $destinationName = 'test_destination';

        $this->dataSourceFactory->expects($this->once())
            ->method('getDataSource')
            ->with($this->equalTo($destinationName))
            ->willReturn($this->makeDataSource([
                'name' => $destinationName,
                'type' => 'Other',
                'user' => 'keys.json',
                'password' => '',
                'host' => 'test_transport',
                'database' => 'main',
            ]));

        $destination = $this->resolver->resolveDestination($destinationName);

        $this->assertEquals('main', $destination->location());
        $this->assertEquals('test_transport', $destination->transport());
        $this->assertEquals('keys.json', $destination->credential());
    }

    public function testCanResolveTransport(): void
    {
        $destination = new Destination('test_destination', 'test_transport', 'home', 'bob', 'sekret');

        $transportMock = $this->createMock(Transport::class);
        $transportMock->expects($this->once())
            ->method('withDestination')
            ->with($this->equalTo($destination))
            ->will($this->returnSelf());

        $this->restngApp->expects($this->once())
            ->method('getService')
            ->with($this->equalTo('FileTransfer.'. $destination->transport()))
            ->willReturn($transportMock);

        $this->assertSame($transportMock, $this->resolver->resolveTransport($destination));
    }
}
