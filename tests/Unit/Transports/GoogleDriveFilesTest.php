<?php

namespace Unit\Transports;

use MiamiOH\FileTransferService\Destination;
use MiamiOH\FileTransferService\File;
use MiamiOH\FileTransferService\Tests\TestCase;
use MiamiOH\FileTransferService\Transports\GoogleDriveFiles;
use MiamiOH\FileTransferService\Transports\GoogleServiceFactory;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

class GoogleDriveFilesTest extends TestCase
{
    /** @var GoogleDriveFiles */
    private $driveFiles;

    /** @var \Google_Client|MockObject */
    private $client;
    /** @var GoogleServiceFactory|MockObject */
    private $serviceFactory;
    /** @var Destination */
    private $destination;
    /** @var \Google_Service_Drive_Resource_Files|MockObject */
    private $filesResource;

    public function setUp(): void
    {
        parent::setUp();

        $this->client = $this->createMock(\Google_Client::class);
        $this->serviceFactory = $this->createMock(GoogleServiceFactory::class);
        $this->destination = new Destination('test_destination', 'test_transport', 'home', 'bob', 'sekret');
        $this->filesResource = $this->createMock(\Google_Service_Drive_Resource_Files::class);

        $this->driveFiles = new GoogleDriveFiles($this->serviceFactory);
    }

    public function testSetsCredentials(): void
    {
        $this->serviceFactory->expects($this->once())
            ->method('setCredentials')
            ->with($this->equalTo($this->destination->credential()));

        $this->driveFiles->withDestination($this->destination);
    }

    public function testClientUsesDefaultCredentials()
    {
        $this->client->expects($this->once())
            ->method('useApplicationDefaultCredentials');

        $this->serviceFactory->expects($this->once())
            ->method('createClient')
            ->willReturn($this->client);

        $this->driveFiles->withDestination($this->destination);
    }

    public function testClientSetsDriveScopes()
    {
        $this->client->expects($this->once())
            ->method('addScope')
            ->with($this->equalTo([\Google_Service_Drive::DRIVE]));

        $this->serviceFactory->expects($this->once())
            ->method('createClient')
            ->willReturn($this->client);

        $this->driveFiles->withDestination($this->destination);
    }

    public function testCreatesFilesResourceFromClient()
    {
        $this->serviceFactory->expects($this->once())
            ->method('createClient')
            ->willReturn($this->client);

        $this->serviceFactory->expects($this->once())
            ->method('createDriveFileResource')
            ->with($this->equalTo($this->client));

        $this->driveFiles->withDestination($this->destination);
    }

    public function testCanListFilesFromGoogleDrive(): void
    {
        $this->prepareFilesResource();

        $fileList = $this->createMock(\Google_Service_Drive_FileList::class);
        $fileList->expects($this->once())
            ->method('getFiles')
            ->willReturn([
                $this->makeDriveFile(['id' => 'xyz321']),
            ]);

        $this->filesResource->expects($this->once())
            ->method('listFiles')
            ->with($this->callback(function (array $optParams) {
                $this->assertEquals(GoogleDriveFiles::MAX_RESULT_LIST_SIZE, $optParams['pageSize']);
                $this->assertEquals("'" . $this->destination->location() . "' in parents", $optParams['q']);
                $this->assertEquals('true', $optParams['supportsTeamDrives']);
                $this->assertEquals('true', $optParams['includeTeamDriveItems']);
                return true;
            }))
            ->willReturn($fileList);

        $collection = $this->driveFiles->listFiles();

        $files = $collection->toArray();
        $this->assertEquals('xyz321', $files[0]->id());
    }

    public function testCanGetFileFromGoogleDrive(): void
    {
        $this->prepareFilesResource();

        $fileData = $this->makeDriveFile(['id' => 'xyz321']);

        $fileBody = $this->createMock(StreamInterface::class);
        $fileBody->expects($this->once())
            ->method('getContents')
            ->willReturn('test content');

        $fileContentResponse = $this->createMock(ResponseInterface::class);
        $fileContentResponse->expects($this->once())
            ->method('getBody')
            ->willReturn($fileBody);

        $this->filesResource->expects($this->exactly(2))
            ->method('get')
            ->withConsecutive(
                [
                    $this->equalTo('xyz321'),
                    $this->callback(function (array $optParams) {
                        $this->assertEquals('true', $optParams['supportsTeamDrives']);
                        $this->assertArrayNotHasKey('alt', $optParams);
                        return true;
                    })
                ],
                [
                    $this->equalTo('xyz321'),
                    $this->callback(function (array $optParams) {
                        $this->assertEquals('true', $optParams['supportsTeamDrives']);
                        $this->assertEquals('media', $optParams['alt']);
                        return true;
                    })
                ]
            )
            ->will($this->onConsecutiveCalls($fileData, $fileContentResponse));

        $this->driveFiles->getFile('xyz321');
    }

    public function testCanCreateFileOnGoogleDrive(): void
    {
        $this->prepareFilesResource();

        $newFile = File::fromArray(['name' => 'test.txt', 'mimeType' => 'text/plain', 'content' => 'test file content']);

        $newDriveFile = $this->createMock(\Google_Service_Drive_DriveFile::class);
        $newDriveFile->expects($this->once())
            ->method('setParents')
            ->with($this->equalTo([$this->destination->location()]));
        $newDriveFile->expects($this->once())
            ->method('setName')
            ->with($this->equalTo($newFile->name()));

        $this->serviceFactory->expects($this->once())
            ->method('createDriveFile')
            ->willReturn($newDriveFile);

        $createdFile = $this->makeDriveFile(['id' => 'abc123', 'name' => 'test.txt', 'mimeType' => 'text/plain']);

        $this->filesResource->expects($this->once())
            ->method('create')
            ->with(
                $this->callback(function (\Google_Service_Drive_DriveFile $subjectFile) use ($newDriveFile) {
                    $this->assertSame($newDriveFile, $subjectFile);
                    return true;
                }),
                $this->callback(function (array $optParams) use ($newFile) {
                    $this->assertEquals($newFile->contentRaw(), $optParams['data']);
                    $this->assertEquals('true', $optParams['supportsTeamDrives']);
                    return true;
                })
            )
            ->willReturn($createdFile);

        $file = $this->driveFiles->createFile($newFile);

        $this->assertEquals('abc123', $file->id());
    }

    public function testCanDeleteFileFromGoogleDrive(): void
    {
        $this->prepareFilesResource();

        $this->filesResource->expects($this->once())
            ->method('delete')
            ->with(
                $this->equalTo('abc123'),
                $this->callback(function (array $optParams) {
                    $this->assertEquals('true', $optParams['supportsTeamDrives']);
                    return true;
                })
            );

        $this->driveFiles->deleteFile('abc123');
    }

    public function tearDown(): void
    {
        parent::tearDown();

        putenv('GOOGLE_APPLICATION_CREDENTIALS');
    }

    private function prepareFilesResource(): void
    {
        $this->serviceFactory->expects($this->once())
            ->method('createDriveFileResource')
            ->willReturn($this->filesResource);

        $this->driveFiles->withDestination($this->destination);
    }

    private function makeDriveFile(array $overrides = []): \Google_Service_Drive_DriveFile
    {
        $defaults = [
            'id' => 'abc123',
            'name' => 'test.txt',
            'mimeType' => 'text/plain',
        ];

        $attributes = array_merge($defaults, $overrides);

        $file = new \Google_Service_Drive_DriveFile();
        $file->id = $attributes['id'];
        $file->name = $attributes['name'];
        $file->mimeType = $attributes['mimeType'];

        return $file;
    }
}
