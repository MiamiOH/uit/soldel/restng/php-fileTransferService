<?php

namespace Unit\Transports;

use MiamiOH\FileTransferService\Destination;
use MiamiOH\FileTransferService\File;
use MiamiOH\FileTransferService\FileCollection;
use MiamiOH\FileTransferService\Tests\TestCase;
use MiamiOH\FileTransferService\Transports\GoogleDriveFiles;
use MiamiOH\FileTransferService\Transports\GoogleDriveTransport;
use PHPUnit\Framework\MockObject\MockObject;

class GoogleDriveTransportTest extends TestCase
{
    /** @var GoogleDriveTransport */
    private $transport;

    /** @var Destination */
    private $destination;
    /** @var GoogleDriveFiles|MockObject */
    private $driveFiles;

    public function setUp(): void
    {
        parent::setUp();

        $this->destination = new Destination('test_destination', 'test_transport', 'home', 'bob',  'sekret');
        $this->driveFiles = $this->createMock(GoogleDriveFiles::class);

        $this->transport = new GoogleDriveTransport($this->driveFiles);
        $this->transport->withDestination($this->destination);
    }

    public function testCanListFilesFromGoogleDrive(): void
    {
        $sampleFiles = new FileCollection([
            File::fromArray(['id' => 'abc123', 'name' => 'test.txt', 'mimeType' => 'text/plain', 'content' => '']),
        ]);

        $this->driveFiles->expects($this->once())
            ->method('listFiles')
            ->willReturn($sampleFiles);

        $files = $this->transport->listFiles();

        $this->assertSame($sampleFiles, $files);
    }

    public function testCanGetFileFromGoogleDrive(): void
    {
        $sampleFile = File::fromArray(['id' => 'abc123', 'name' => 'test.txt', 'mimeType' => 'text/plain', 'content' => '']);

        $this->driveFiles->expects($this->once())
            ->method('getFile')
            ->willReturn($sampleFile);

        $file = $this->transport->getFile('abc123');

        $this->assertSame($sampleFile, $file);
    }

    public function testCanCreateFileOnGoogleDrive(): void
    {
        $fileData = ['name' => 'test.txt', 'mimeType' => 'text/plain', 'content' => 'test content'];
        $sampleFile = File::fromArray(array_merge(['id' => 'abc123'], $fileData));

        $this->driveFiles->expects($this->once())
            ->method('createFile')
            ->willReturn($sampleFile);

        $file = $this->transport->createFile(File::fromArray($fileData));

        $this->assertEquals($sampleFile->toArray(), $file->toArray());
        $this->assertNotEmpty($file->id());
    }

    public function testCanDeleteFileFromGoogleDrive(): void
    {
        $this->driveFiles->expects($this->once())
            ->method('deleteFile')
            ->with($this->equalTo('abc123'));

        $this->transport->deleteFile('abc123');
    }
}
