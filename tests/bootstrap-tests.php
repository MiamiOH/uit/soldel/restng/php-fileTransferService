<?php

require __DIR__ . '/../vendor/autoload.php';

// This works around a problem running tests on containers with PhpStorm.
// PhpStorm changes the cwd and leaves us in the wrong location.
chdir(__DIR__ . '/..');
